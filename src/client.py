import cv2 as cv

from mask import DefaultMask

window_capture_name = 'Video Capture'
window_detection_name = 'Object Detection'
cv.namedWindow(window_capture_name)
cv.namedWindow(window_detection_name)

cap = cv.VideoCapture(0)
mask = DefaultMask()
mask = mask.into_dyn(cv)

while True:
    
    ret, frame = cap.read()
    if frame is None:
        break


    frame_HSV = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    frame_threshold = cv.inRange(frame_HSV, (mask.min_h, mask.min_s, mask.min_v), (mask.max_h, mask.max_s, mask.max_v))
    
    
    cv.imshow(window_capture_name, frame)
    cv.imshow(window_detection_name, frame_threshold)
    
    key = cv.waitKey(30)
    if key == ord('q') or key == 27:
        break