from configparser import ConfigParser
import sys
import os.path
from pathlib import Path

TARGET_SECTION_NAME = 'MASK'

class Config:
    """
    Upon initialization, this should crash if the config file is not found.
    ...

    Attributes
    ----------


    Methods
    -------
    init(path : str) -> self
        Dependency injection pattern (Effective Java: Item 5) is used here as different users might have different config file names.
        Must use Python's `with` clause, based on [this](https://stackoverflow.com/a/2843781/8558585).

        Raises
        ------
        OSError
                This is the error raised by using Python's built-in function `open()`. Read [source](https://docs.python.org/3/library/functions.html#open)
        DuplicateOptionError
                Raised if duplicate keys are found in the config file

    """

    def __init__(self, path: str):
        self.config = ConfigParser()
        self.config.read_file(open(path))

    def mask_value_keys_set(self) -> set:
        return frozenset(self.config.options(TARGET_SECTION_NAME))
    
    def get_mask(self, key) -> int:
        return int(self.config[TARGET_SECTION_NAME][key])


if __name__ == "__main__":
    print()
    print("running tests on:", sys.modules[__name__].__file__, "\n")
    c = Config("./test.ini")
    res = c.mask_value_keys_set()
    print(res)
    print()

    print("Testing a non-existent file")
    try:
        c = Config("./non_existent_file")
    except FileNotFoundError:
        print("Config() catched FileNotFoundError exception")

    print("unit testing done\n")
