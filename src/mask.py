from enum import Enum, auto
import sys
import cv2

from config import Config

CONFIG_FILE_PATH = "./test.ini"
WINDOW_DETECTION_NAME = 'Object Detection'

class MaskModel(Enum):
    MIN_H = auto()
    MIN_S = auto()
    MIN_V = auto()
    MAX_H = auto()
    MAX_S = auto()
    MAX_V = auto()

    @staticmethod
    def vars_set_mut() -> set:
        return set([name.lower() for name, _ in MaskModel.__members__.items()])


class DefaultMask:
    """
    When instantiated, the config file which contains the default values for
    the mask is searched, read, and its values saved in this class' data members.
    If config file is not found the program should crash.

    I'm not satisfied with this solution but this'll do for now.
    ...

    Attributes
    ----------
    The following attributes are set at runtime:
        min_h : int
        min_s : int
        min_v : int
        max_h : int
        max_s : int
        max_v : int

    Methods
    -------
    init(path=None) -> new self
        Dependency injection pattern (Effective Java: Item 5) is used here as different users might have different config file names.

        Raises
        ------
        KeyError
            This would be raised if
            Number of keys found in the config file is not exactly equal to the number of keys that is required.
            or
            There is a discrepancy on the naming of keys in the config file


    into_dyn(self) -> DynamicMask
        DefaultMask() would then be overwritten to the address it was initially assigned in.

    """

    def __init__(self, path=CONFIG_FILE_PATH):
        c = Config(path)

        vars_from_config_set = c.mask_value_keys_set()
        should_be_vars = MaskModel.vars_set_mut()
        if len(should_be_vars) != len(vars_from_config_set):
            raise KeyError("Number of keys found in the config file is not exactly equal to the number of keys that is required.")

        if len(should_be_vars.symmetric_difference(vars_from_config_set)) != 0:
            raise KeyError("There is a discrepancy on the naming of keys in the config file")

        while True:
            try:
                var = should_be_vars.pop()
                setattr(self, f'''{var}''', c.get_mask(var))
            except KeyError:
                break


    def into_dyn(self, cv):
        return DynamicMask(self.__dict__, cv)


class DynamicMask:
    """
    This mainly uses `cv::createTrackbar` instead of the default values in the
    config file used in DefaultMask()

    ...

    Attributes
    ----------
    The following attributes are set at runtime:
        min_h : int
        min_s : int
        min_v : int
        max_h : int
        max_s : int
        max_v : int

    Methods
    -------
    __init__(starting_vals: dict) -> new self
        `starting_vals` would be the attributes in `DefaultMask()` from
        which this class is instantiated

        Raises
        ------
        KeyError
            If key is not of a type `str`
        
        ValueError
            Value to which a Key is mapped should be an int type

        IOError
            Invalid value instantiated

    into_def(self) -> DefaultMask
        DynamicMask() would then be overwritten to the address it was initially assigned in.

    render_trackbars()
        creates a `cv2` window that holds trackbars to adjust mask values at runtime

    """

    def __init__(self, starting_vals, cv_obj):
        for variable, value in starting_vals.items():
            if not isinstance(variable, str):
                raise KeyError("Key should be a type `str`")

            if not isinstance(value, int):
                raise ValueError("Value to which a Key is mapped should be an int type")
            
            if value > 255 or value < 0:
                raise IOError("Invalid value instantiated")

            setattr(self, f'''{variable}''', value)

        self.cv = cv_obj
        self.render_trackbars()
        

    def render_trackbars(self):
        self.cv.createTrackbar(MaskModel.MIN_H.name, WINDOW_DETECTION_NAME, 0, 255, self.update_min_h)
        self.cv.createTrackbar(MaskModel.MIN_S.name, WINDOW_DETECTION_NAME, 0, 255, self.update_min_s)
        self.cv.createTrackbar(MaskModel.MIN_V.name, WINDOW_DETECTION_NAME, 0, 255, self.update_min_v)

        self.cv.createTrackbar(MaskModel.MAX_H.name, WINDOW_DETECTION_NAME, 0, 180, self.update_max_h)
        self.cv.createTrackbar(MaskModel.MAX_S.name, WINDOW_DETECTION_NAME, 0, 255, self.update_max_s)
        self.cv.createTrackbar(MaskModel.MAX_V.name, WINDOW_DETECTION_NAME, 0, 255, self.update_max_v)

    def update_min_h(self, val):
        self.min_h = val

    def update_min_s(self, val):
        self.min_s = val

    def update_min_v(self, val):
        self.min_v = val

    def update_max_h(self, val):
        self.max_h = min(val, 180)

    def update_max_s(self, val):
        self.max_s = val

    def update_max_v(self, val):
        self.max_v = val


    def into_def(self) -> DefaultMask:
        return DefaultMask(CONFIG_FILE_PATH)



class Mask:
    """
    This would serve as the main interface for the client.
    At initialization, DefaultMask() would be used.

    This is a noninstantiable class (Effective Java: Item 4) with a static factory method (Effective Java: Item 1)
    that instantiates other classes to which the client doesn't have to know.
    As of this writing, I'm having a hard time defining interface (Java) or traits (Rust)
    in Python. I'm not comfortable with the implementation that the user wouldn't know
    that the data structure they're using is mutating implicitly.
    ...

    Attributes
    ----------

    Methods
    -------
    init() -> DefaultMask


    ======================
    2022-02-22 16:27
    The current implementation deviated from the initial intention. `DefaultMask.into_dyn()`
    should mutate itself, not return a new object. And similarly, `DynamicMask.into_def()`
    should mutate itself into `DefaultMask`. Current problem is lacking knowledge on
    how to implement the aforementioned structure. The current implementation will perhaps
    remain for now.

    """

    pass


if __name__ == "__main__":
    print()
    print("running tests on:", sys.modules[__name__].__file__)
    print(MaskModel.vars_set_mut())
    print()

    print()
    print("Running default mask")
    mask = DefaultMask()
    print("min_h:", mask.min_h)
    print("min_s:", mask.min_s)

    print()
    print("Running faulty default mask: missing key")
    try:
        mask = DefaultMask("./missing_var.ini")
    except KeyError as e:
        print("DefaultMask() error catched:", e)


    print()
    print("Running faulty default mask: misnamed key")
    try:
        mask = DefaultMask("./misnamed.ini")
    except KeyError as e:
        print("DefaultMask() error catched:", e)
    print()
